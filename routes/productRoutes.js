const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");

// Get - Get All Active Products (Retrieve all active products)
router.get("/getAllActiveProducts", (req, res) => {

	productController.getAllActive().then(resultFromAllActive => res.send(resultFromAllActive));

})

// Get - Get Specific Product (Retrieve single product)
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromGetSpecificProduct => res.send(resultFromGetSpecificProduct))

})

// Put - Update Product Info (Update Product information - Admin only)
router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin)
	{

		productController.updateProduct(req.params, req.body).then(resultFromUpdateProduct => res.send(resultFromUpdateProduct))
	}
	else
	{
		res.send({auth: "Failed to update product, user not an admin"})
	}

})

// Put - Archive Product (Archive Product - Admin only)
router.delete("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin)
	{
		productController.archiveProduct(req.params).then(resultFromArchiveProduct => res.send(resultFromArchiveProduct))
	}
	else
	{
		res.send({auth: "Failed to archive product, user not an admin"})
	}
})


module.exports = router;