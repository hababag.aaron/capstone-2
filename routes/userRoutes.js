const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");


// Post - Register User (User registration)
// Route for registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister));

});

// Post - Authenticate User (User authentication)
// Route for loging-in
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin));

})

// Route to check for duplicate emails
router.post("/checkEmail", (req, res) => {

	userController.checkEmail(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists));

})

// Route for getting user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(userData.id).then(resultFromgetProfile => res.send(resultFromgetProfile));

})

// Post - Create Product (Create Product - Admin only)
router.post("/addProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin)
	{

		userController.addProducts(req.body).then(resultFromAddProducts => res.send(resultFromAddProducts));
	}
	else
	{
		res.send({auth: "Failed to add product user, not an admin"})
	}

})

// Post - Checkout (Non-admin User checkout - Create Order)
router.post("/createOrders", auth.verify, (req, res) => {

	const allData = {

		userId : auth.decode(req.headers.authorization).id,
		orderedItem : req.body.orderedItem,
		totalQuantity : req.body.totalQuantity,
		totalAmount : req.body.totalAmount
	}

	userController.createOrders(allData).then(resultFromOrder => res.send(resultFromOrder))

})


// Get - Get my Orders (Retrieve authenticated user’s orders)
router.get("/getMyOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	userController.getMyOrders(userData.id).then(resultFromGetOrders => res.send(resultFromGetOrders))

})

// Get - Get All Orders (Retrieve all orders - Admin only)
router.get("/getAllOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin)
	{
		userController.getAllOrders().then(resultFromAllOrders => res.send(resultFromAllOrders))
	}
	else
	{
		res.send({auth: "Failed to get all orders, not an admin"})
	}
})


// Put - Set User as Admin (STRETCH)
router.put("/setUserAsAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin)
	{
		userController.setAsAdmin(req.body).then(resultFromSetAdmin => res.send(resultFromSetAdmin))
	}
	else
	{
		res.send({auth: "Failed to set as an admin, your credential is not an admin"})
	}	

})

router.put("/setUserAsNotAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin)
	{
		userController.setAsNotAdmin(req.body).then(resultFromSetNotAdmin => res.send(resultFromSetNotAdmin))
	}
	else
	{
		res.send({auth: "Failed to set as not an admin, your credential is not an admin"})
	}


})



module.exports = router;