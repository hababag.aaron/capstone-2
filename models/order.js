const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: true
	},

	orderedItem: [
		{
			productId : {
				type: String,
				required: [true, "Product ID is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			}
		}
	],

	totalQuantity : {
		type : Number,
		required : [true, "Total Quantity is required."]
	},

	totalAmount: {
		type: Number,
		required: [true, "Price is required."]
	},

	orderCreatedOn: {
		type: Date,
		default: new Date()
	}


})

module.exports = mongoose.model("Order", orderSchema);