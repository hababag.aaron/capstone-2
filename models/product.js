const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	productCode : {
		type :String,
		required : [true, "Product code is required"]
	},
	productName : {
		type : String,
		required : [true, "Product name is required"]	
	},
	prodDescription : {
		type : String,
		required : [true, "Description is required"]
	},
	category : {
		type : String,
		required : [true, "Category is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	quantity : {
		type : Number,
		required: [true, "Quantity is required"]
	},
	isAvailable : {
		type : Boolean,
		default : true
	},
	attributes : {

		size : String,
		color : String,
		waist : String,
		material : String,
	}


})

module.exports = mongoose.model("Product", productSchema);