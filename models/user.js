const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	lastName : {
		type : String,
		required : [true, "First name is required"]
	},
	firstName : {
		type : String,
		required : [true, "First name is required"]	
	},
	middleName : {
		type : String,
		required : false
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile number is required"]
	}
	
})

module.exports = mongoose.model("User", userSchema);