const jwt = require("jsonwebtoken");
const secretKey = "ProductAPI";

module.exports.createAccessToken = (user) => {

	const data = {

		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}

	return jwt.sign(data, secretKey, {});
}


// Verification of the token (Analogy: receive the gift and verify if the sender is legitimate and the gift was not tampered with)

module.exports.verify = (req, res, next) => {

	// Get our JSONWebToke, which is found in the authorization header
	let token = req.headers.authorization

	if(typeof token !== "undefined")
	{
		console.log(token)
		// slice removes the unnecessary. "Bearer" part of out token
		token = token.slice(7, token.length);

		return jwt.verify(token, secretKey, (err, data) => {

			if(err)
			{
				return res.send({auth : "failed"})
			}
			else
			{
				next();
			}
		})

	}
	else
	{
		return res.send({auth : "failed"})
	}

}

// Decoding of the token (analogy: open the gift and get the contents)
module.exports.decode = (token) => {

	// check if token is present
	if(typeof token !== "undefined")
	{
		token = token.slice(7, token.length);

		return jwt.verify(token, secretKey, (err, data) => {
			if (err) 
			{
				return null;
			}
			else
			{
				// complete : true means do a completed decoding of the token
				// payload is the data from the token when created the access token
				return jwt.decode(token, {complete : true}).payload;
			}


		})
	}
	else
	{
		// no token
		return null;
	}

}

