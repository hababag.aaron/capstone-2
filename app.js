// Set up the dependencies
// Require is used to relate others files to our current file
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// import routes
const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
// const orderRoutes = require("./routes/orderRoutes");

// Server setup
const app = express(); 
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended : true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

mongoose.connect("mongodb+srv://admin:admin123@cluster0.65wvh.mongodb.net/tshirt_order_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology : true
});

mongoose.connection.once("open", () => {
	console.log("Now connected to MongoDB Atlas.")
});

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
});