const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.registerUser = (body) => {

	return User.find({email : body.email}).then(result => {

		if(result.length > 0)
		{
			return false;
		}
		else
		{

			let newUser = new User({

				firstName : body.firstName,
				lastName : body.lastName,
				middleName : body.middleName,
				email : body.email,
				mobileNo : body.mobileNo,
				password : bcrypt.hashSync(body.password, 10),
			})

			return newUser.save().then((user, error) => {

				if (error) 
				{
					return false;
				}
				else
				{
					return true;
				}

			})
		}
		
	})
	// let newUser = new User({

	// 	firstName : body.firstName,
	// 	lastName : body.lastName,
	// 	middleName : body.middleName,
	// 	email : body.email,
	// 	mobileNo : body.mobileNo,
	// 	password : bcrypt.hashSync(body.password, 10),
	// })

	// return newUser.save().then((user, error) => {

	// 	if (error) 
	// 	{
	// 		return false;
	// 	}
	// 	else
	// 	{
	// 		return true;
	// 	}

	// })

}


module.exports.loginUser = (body) => {

	return User.findOne({email : body.email}).then(result => {

		if (result === null) 
		{
			// user doesn't exist
			return false;
		}
		else
		{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password);

			if (isPasswordCorrect) 
			{
				return {access : auth.createAccessToken(result.toObject())}
			}
			else
			{	
				// password didn't match
				return false;
			}
		}

	})

}

module.exports.checkEmail = (body) => {

	return User.find({email : body.email}).then(result => {

		if (result.length > 0) 
		{
			// there is duplicate
			return true;
		}
		else
		{
			// no duplicate
			return false;
		}
	})

}

module.exports.getProfile = (userId) => {

	return User.findById(userId).then(result => {

		// reassign the value of the user's password to undefined for security purposes
		result.password = undefined;

		return result;

	})

}

module.exports.addProducts = (body) => {

	let newProduct = new Product({

		productCode : body.productCode,
		productName : body.productName,
		prodDescription : body.prodDescription,
		category : body.category,
		price : body.price,
		quantity : body.quantity,
		attributes : {

			size : body.attributes.size,
			color : body.attributes.color,
			waist : body.attributes.waist,
			material : body.attributes.material
		}
	})

	return newProduct.save().then((user, error) => {

		if (error) 
		{
			return false;
		}
		else
		{
			return true;
		}

	})

}

module.exports.createOrders = async (data) => {

	let newOrder = new Order({

		userId : data.userId,
		orderedItem : data.orderedItem,
		totalQuantity : data.totalQuantity,
		totalAmount : data.totalAmount
	})


	return newOrder.save().then((user, error) => {

		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}
	})
}

module.exports.getMyOrders = (dUserId) => {

	return Order.find({userId : dUserId}).then(result => {

		return result;

	})

}


module.exports.getAllOrders = () => {

	return Order.find().then(result => {

		return result;
	})
}


module.exports.setAsAdmin = (body) => {

	let setAdmin = {

		isAdmin : true

	}

	return User.findByIdAndUpdate(body.userId, setAdmin).then((user, error) => {


		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}
	})

}

module.exports.setAsNotAdmin = (body) => {

	let setAdmin = {

		isAdmin : false

	}

	return User.findByIdAndUpdate(body.userId, setAdmin).then((user, error) => {


		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}
	})

}