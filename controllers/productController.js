const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/product");
const auth = require("../auth");
const bcrypt = require("bcrypt");

module.exports.getAllActive = () => {

	return Product.find({isAvailable : true}).then(result => {

		return result;
	})

}

module.exports.getProduct = (params) => {

	return Product.findById(params.productId).then(result => {

		return result;
	})
}

module.exports.updateProduct = (params, body) => {

	let updateProduct = {

		productCode : body.productCode,
		productName : body.productName,
		prodDescription : body.prodDescription,
		category : body.category,
		price : body.price,
		quantity : body.quantity,
		attributes : {

			size : body.attributes.size,
			color : body.attributes.color,
			waist : body.attributes.waist,
			material : body.attributes.material
		}
	}

	return Product.findByIdAndUpdate(params.productId, updateProduct).then((product, error) => {

		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}

	})
}


module.exports.archiveProduct = (params) => {

	let deleteProduct = {

		isAvailable : false,
		quantity : 0

	}

	return Product.findByIdAndUpdate(params.productId, deleteProduct).then((course, error) => {

		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}

	})
}

